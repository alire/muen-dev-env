all: .env

.get-toolchain:
	alr toolchain --select gnat_native=12.2.1
	alr toolchain --select gprbuild=22.0.1
	@touch $@

.build-deps: .get-toolchain alire.toml
	alr build
	@touch $@

.env: .build-deps
	alr printenv > $@
